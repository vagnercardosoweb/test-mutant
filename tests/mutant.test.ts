import app from '../src/app';
import request from 'supertest';

describe('[GET] /', () => {
  it('Recupera a página inicial.', async () => {
    try {
      const response = await request(app).get('/');
      expect(response.status).toBe(200);
      expect(response.body).toBeInstanceOf(Object);
      expect(response.body.developer).toBe('Vagner dos Santos Cardoso');
    } catch (error) {
      return fail(error);
    }
  });
});

describe('[GET] /notfound', () => {
  it('Testa rota que não existe.', async () => {
    try {
      const response = await request(app).get('/notfound');
      expect(response.status).toBe(404);
      expect(response.body.message).toBe('Error 404 (Not Found)');
    } catch (error) {
      return fail(error);
    }
  });
});
