import { UserInterface } from '../src/interfaces/UserInterface';
import app from '../src/app';
import request from 'supertest';

describe('[GET] /users', () => {
  it('O Nome, email e a empresa em que trabalha (em ordem alfabética).', async () => {
    try {
      const response = await request(app).get('/users');
      expect(response.status).toBe(200);
      expect(response.body).toBeInstanceOf(Array);
      expect(response.body[0].name).toBeDefined();
      expect(response.body[0].email).toBeDefined();
      expect(response.body[0].company).toBeDefined();
    } catch (error) {
      return fail(error);
    }
  });
});

describe('[GET] /address', () => {
  it('Mostrar todos os usuários que no endereço contem a palavra `suite`', async () => {
    try {
      const response = await request(app).get('/address');
      expect(response.status).toBe(200);
      expect(response.body).toBeInstanceOf(Array);
      expect(response.body[0]).toBeDefined();

      (<UserInterface[]>response.body).forEach(user => {
        expect(user.address.suite).toMatch(/suite/gi);
      });
    } catch (error) {
      return fail(error);
    }
  });
});

describe('[GET] /websites', () => {
  it('Os websites de todos os usuários', async () => {
    try {
      const response = await request(app).get('/websites');
      expect(response.status).toBe(200);
      expect(response.body).toBeInstanceOf(Array);
      expect(response.body[0]).toBeDefined();
    } catch (error) {
      return fail(error);
    }
  });
});
