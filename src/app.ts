import elasticApm from 'elastic-apm-node';
import express, { Response, Request } from 'express';

import routes from './routes';

class App {
  public express: express.Application;

  public constructor() {
    this.apm();

    this.express = express();

    this.routes();
    this.exception();
  }

  private apm(): void {
    // elastic
    // OcCwJbhupNuR8Oj0gNELmhfJ

    if (
      process.env.NODE_ENV !== undefined &&
      process.env.NODE_ENV !== 'testing'
    ) {
      elasticApm.start({
        secretToken: 'KXhu3oNmerrgYAs6sq',
        serviceName: 'APM Test Mutant',
        serverUrl:
          'https://b25ea670b81942e3babed6cde9332f74.apm.asia-northeast1.gcp.cloud.es.io:443'
      });
    }
  }

  private routes(): void {
    this.express.use(routes);
    this.express.use(
      (req: Request, res: Response): Response => {
        return res.status(404).json({
          error: true,
          message: 'Error 404 (Not Found)'
        });
      }
    );
  }

  private exception(): void {
    this.express.use(
      (err, req: Request, res: Response): Response => {
        return res.status(500).json({
          error: true,
          message: 'Internal Server Error.'
        });
      }
    );
  }
}

export default new App().express;
