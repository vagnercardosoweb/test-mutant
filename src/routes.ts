import { Router } from 'express';

import MutantController from './controllers/MutantController';
import UserController from './controllers/UserController';

const routes = Router();

routes.get('/', MutantController.index);
routes.get('/users', UserController.index);
routes.get('/address', UserController.address);
routes.get('/websites', UserController.websites);

export default routes;
