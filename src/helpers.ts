import { UserInterface } from './interfaces/UserInterface';

export function orderByUserAlphabetic(a: UserInterface, b: UserInterface) {
  if (a.name < b.name) {
    return -1;
  } else if (a.name > b.name) {
    return 1;
  }

  return 0;
}
