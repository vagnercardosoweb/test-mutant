import app from './app';

app.listen(8080, () =>
  console.log(`[SERVER] Running at http://localhost:8080`)
);
