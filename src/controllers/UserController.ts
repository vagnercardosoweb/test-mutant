import UserService from '../services/UserService';
import { Request, Response } from 'express';
import { UserInterface } from '../interfaces/UserInterface';
import { orderByUserAlphabetic } from '../helpers';

class WebSiteController {
  async index(req: Request, res: Response) {
    try {
      const users: UserInterface[] = await UserService.fetchAll();

      let reduceUsers = users
        .reduce((array, user) => {
          array.push({
            name: user.name,
            email: user.email,
            company: user.company && user.company.name
          });

          return array;
        }, [])
        .sort(orderByUserAlphabetic);

      return res.status(200).json(reduceUsers);
    } catch (err) {
      return res.status(400).json({
        message: err.message || 'Erro ao listar os usuários.'
      });
    }
  }

  async address(req: Request, res: Response) {
    try {
      let users: UserInterface[] = await UserService.fetchAll();

      const filtered = users.filter(
        user => user.address && user.address.suite.match(/suite/gi)
      );

      return res.status(200).json(filtered);
    } catch (err) {
      return res.status(400).json({
        message: err.message || 'Erro ao listar os usuários.'
      });
    }
  }

  async websites(req: Request, res: Response) {
    try {
      const websites: [] = [];
      const users: UserInterface[] = await UserService.fetchAll();

      users
        .filter(user => user.website)
        .map(user => websites.push(<never>user.website));

      return res.status(200).json(websites);
    } catch (error) {
      return res.status(400).json({ error });
    }
  }
}

export default new WebSiteController();
