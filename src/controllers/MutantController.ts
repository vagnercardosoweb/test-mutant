import { Request, Response } from 'express';

class MutantController {
  index(req: Request, res: Response) {
    return res.status(200).json({
      date: new Date(),
      company: 'Mutant - Especialistas em Customer Experience (CX)',
      subject: 'Desafio nodejs',
      developer: 'Vagner dos Santos Cardoso'
    });
  }
}

export default new MutantController();
