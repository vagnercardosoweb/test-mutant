import HttpService from './HttpService';
import { UserInterface } from '../interfaces/UserInterface';

class UserService extends HttpService {
  public fetchAll(): Promise<UserInterface[]> {
    return this.createRequest({
      host: 'jsonplaceholder.typicode.com',
      path: '/users',
      method: 'GET'
    });
  }
}

export default new UserService();
