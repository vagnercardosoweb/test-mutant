# Defario Mutant

Código referente ao defafio enviado via e-mail na data **21/08/2019 às 15:51** horário oficial de Brasília.

Segue abaixo as instruções de como rodar o projeto tanto em **Docker** e **Local**.

## Técnologias

- [NodeJS](https://nodejs.org/en/)
- [PM2](http://pm2.keymetrics.io/) para gerenciar os processos em ambiente de produção (docker).
- [Express JS](https://expressjs.com/) para trabalhar com request, response, middleare.
- [TypeScript](https://www.typescriptlang.org/) para colocar tipagem estática no javascript.
- [Jest](https://jestjs.io/) para realizar os testes.
- [Supertest](https://github.com/visionmedia/supertest) para auxiliar na criação dos testes.
- [ESLint](https://eslint.org/) para ter um padrão de código a seguir no projeto.
  - Helpers do eslint.
    - [@typescript-eslint/parser](https://www.npmjs.com/package/@typescript-eslint/parser)
    - [eslint-config-airbnb-base](https://www.npmjs.com/package/eslint-config-airbnb-base)
    - [eslint-config-prettier](https://www.npmjs.com/package/eslint-config-prettier)
    - [eslint-plugin-import](https://www.npmjs.com/package/eslint-plugin-import)
    - [eslint-plugin-node](https://www.npmjs.com/package/eslint-plugin-node)
    - [eslint-plugin-prettier](https://www.npmjs.com/package/eslint-plugin-prettier)

## Observações

O desafio foi a construção de uma api rest recuperando os dados de [Usuários](https://jsonplaceholder.typicode.com/users)
e realizar algumas manipulações nos dados e retornar novamente um JSON com os desafíos compridos.

---

## Preparação

Para utilização do código fonte deve levar em conta que se for rodar em **Docker** é necessário a [instalação](http://docs.docker.com/install/) do mesmo para poder executar o código com sucesso.

Caso queira executar o código em sua máquina diretamente é necessário ter instalado em seu computador ou notebook o [NodeJS](https://nodejs.org/en/) e com isso ele já vem com o pacote **npm** instalado.

Caso queira usar o **yarn** e não tem instalado em sua máquina é necessário rodar o seguinte comando em seu **terminal** de preferência.

```bash
$ npm install -g yarn
```

O comando acima irá instalar globalmente em sua máquina o **yarn** e caso esteja usando o sistema operacional [Linux](https://ubuntu.com/) pode ser necessário colocar o **sudo** antes do comando ficando da seguinte forma:

```bash
$ sudo npm install -g yarn
```

---

## Preparando o projeto

Primeiramente você deve fazer o [download](https://bitbucket.org/vagnercardosoweb/test-mutant/get/a24865d25fd6.zip) do repositório e descompatar ou clonar em sua máquina.

### Clonando o projeto

Para clonar o repositório você deve abrir um **terminal** de sua preferência e navegue ao diretório de sua preferência e ter o [git](https://git-scm.com/) instalado em sua máquina, após isso é só rodar o seguinte comando:

```bash
$ git clone git@bitbucket.org:vagnercardosoweb/test-mutant.git --depth=1
```

O comando acima irá criar uma pasta chamada **test-mutant** no diretório que você escolheu.

### Baixando o projeto

Ao baixar o repositório você deve descompactar e nisso irá gerar uma pasta também em sua máquina com o nome parecido **vagnercardosoweb-test-mutant-a24865d25fd6** ou igual.

---

## Utilizando o Docker

Nesse ponto você já tem em sua máquina o Docker instalado **caso prefira utilizar ele** e já o respositório baixado ou clonado em sua máquina.

Você deve abrir um **terminal** de sua preferência e navegar até a pasta que você **baixou/clonou** o repositório e rodar os seguinte comando para inicializar o servidor.

```bash
$ docker-compose up
```

O Comando acima pode demorar um pouco dependendo da sua conexão com a internet, ele irá baixar imagem do **nodejs** e preparar todo ambiente para daí iniciar o servidor e ao final de tudo você deve ter na saída de seu **terminal** algo parecido com a foto abaixo.

![](assets/docker-compose-terminal.png)

Para parar o servidor (doker) é so aperta Ctrl + C no Windows/Linux ou Command + C no Mac.

---

## Utilizando sua máquina

**Para isso deverá ter o node instalado com npm ou yarn**.

Você deve abrir um **terminal** de sua preferência e navegar até a pasta que você **baixou/clonou** o repositório e rodar os seguintes comandos em ordem para inicializar o servidor node.

```bash
# Utilizando npm
$ npm install
$ npm run start

# Utilizando yarn
$ yarn install
$ yarn start
```

O **1° comando** pode demorar um pouco dependendo de sua conexão pois ele irá realizar o download das bibliotecas que foi utilizada no desafio.

Já o **2° Comando** apenas irá inicializar o servidor node.

Você deve ver no terminal uma saída parecida com a imagem abaixo.

![](assets/servidor-local-terminal.png)

Para parar o servidor (node) é so aperta Ctrl + C no Windows/Linux ou Command + C no Mac.

---

## Acessando os endpoints (endereços webs)

Conforme nas imagens acima na saida do **terminal** a mensagem **[SERVER] Running at http://localhost:8080** e o sinal que o projeto já está online e basta apenas você clicar no link ou copiar e colar em seu navegador de preferência **no meu caso utilizo o Google Chorme**.

**Todos os retornos da api é em JSON**.

Eu utilizo no Google Chorme um Plugin para formatação de **JSON** para não ficar apenas texto em tela e ser mais visivel para poder olhar, o plugin se chame-se [JSON Viewer](https://chrome.google.com/webstore/detail/json-viewer/gbmdgpbipfallnflgajpaliibnhdgobh) e existe outros também e isso vai da preferência da pessoa e também não tendo um Plugin para tratar o **JSON** não há problema algum, eu irei mostrar a imagem com o Plugin mais oque importa é os dados em sí e não o visual agradável.

### Página incial | http://localhost:8080/

Essa página irá listar apenas um **JSON** como na imagem abaixo.

![](assets/tela-inicial.png)

### Os websites de todos os usuários | http://localhost:8080/websites

Lista um **JSON** com os websites de todos usuários

![](assets/tela-websites.png)

### O Nome, email e a empresa em que trabalha (em ordem alfabética). | http://localhost:8080/websites

Lista um **JSON** filtrando apenas nome, e-mail e a empresa que o usuário trabalha em order alfabética pelo **name**.

![](assets/tela-usuarios.png)

### Mostrar todos os usuários que no endereço contem a palavra `suite` | http://localhost:8080/address

Lista um **JSON** filtrando os usuários que contém a palavra **suite** no endereço e no caso esse filtro retorna todos dados do usuário sem remoção de íncides ou adição.

**OBS:** ao meu entender eu filtrei pelo íncide **address -> suite**.

![](assets/tela-address.png)

---

## Elasticsearch APM

Foi adicionado o [APM](https://www.elastic.co/pt/products/apm) para o monitoramento da aplicação.

**Não tenho tanto conhecimento ainda em Elasticsearch mais tentei fazer o mais simples possível para não fazer algo errado**

Como poderá rodar tanto o projeto docker e na máquina pessoal teria que ter tudo instalado na máquina e no docker e para facilitar eu criei o servidor na nuvem do Elasticsearch e para acessar o Kibana siga as instruções abaixo.

- [Realizar login](<https://2deafaf6a0a94a58b500479cd06cef68.asia-northeast1.gcp.cloud.es.io:9243/login#?_g=()>)
  - Username: **elastic**
  - Password: **OcCwJbhupNuR8Oj0gNELmhfJ**

Apos realizar o login acesse [Discover](https://2deafaf6a0a94a58b500479cd06cef68.asia-northeast1.gcp.cloud.es.io:9243/app/kibana#/discover) para ver os logs.

**Como dito acima não tenho tanto conhecimento e se fiz algo errado não foi da minha intenção**

---

## Testes únitários

Para realizar os testes é muito simples basta rodar um comando como mostrado abaixo em seu **terminal** de preferência.

### Utilizando o docker

```bash
# Utilizando npm
$ docker exec -i docker-test-mutant sh -c "npm run test"

# Utilizando yarn
$ docker exec -i docker-test-mutant sh -c "yarn test"
```

E na saída do seu terminal deverá aparecer algo parecido com a imagem abaixo.

![](assets/run-teste-docker.png)

### Utilizando sua máquina

**Para isso deverá ter o node instalado com npm ou yarn**.

```bash
# Utilizando npm
$ npm run test

# Utilizando yarn
$ yarn test
```

E na saída do seu terminal deverá aparecer algo parecido com a imagem abaixo.

![](assets/run-teste-local.png)

---

Se chegou até aqui sua aplicação foi mostrada e testada com sucesso! Obrigado.

Atenciosamento, Vagner dos Santos Cardoso.
