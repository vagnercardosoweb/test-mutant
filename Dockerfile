FROM node

RUN apt-get update && \
  apt-get upgrade -y

RUN yarn global add pm2
RUN yarn global add typescript

RUN mkdir -p /home/node/mutant
RUN chown -R node:node /home/node/mutant

COPY package.json ./
RUN yarn

COPY . ./
COPY --chown=node:node . ./

RUN chown node:1000 -R /home/node/mutant

USER node

CMD ["yarn", "start"]
